$(document).ready(function(){
	/*Navegação*/
	$("#op-bg").click(function(){
		$(".span10").children().hide();
		$("#bg").fadeIn(1000);
		$("li").removeClass("active");
		$(this).parent().addClass("active");
	});

	$("#op-face").click(function(){
		$(".span10").children().hide();
		$("#face").fadeIn(1000);
		$("li").removeClass("active");
		$(this).parent().addClass("active");
	});

	$("#op-olhos").click(function(){
		$(".span10").children().hide();
		$("#olhos").fadeIn(1000);
		$("li").removeClass("active");
		$(this).parent().addClass("active");
	});

	$("#op-nariz").click(function(){
		$(".span10").children().hide();
		$("#nariz").fadeIn(1000);
		$("li").removeClass("active");
		$(this).parent().addClass("active");
	});

	$("#op-boca").click(function(){
		$(".span10").children().hide();
		$("#boca").fadeIn(1000);
		$("li").removeClass("active");
		$(this).parent().addClass("active");
	});

	$("#op-cabelo").click(function(){
		$(".span10").children().hide();
		$("#cabelo").fadeIn(1000);
		$("li").removeClass("active");
		$(this).parent().addClass("active");
	});

	$("#op-roupa").click(function(){
		$(".span10").children().hide();
		$("#roupa").fadeIn(1000);
		$("li").removeClass("active");
		$(this).parent().addClass("active");
	});

	$("#op-acessorio").click(function(){
		$(".span10").children().hide();
		$("#acessorio").fadeIn(1000);
		$("li").removeClass("active");
		$(this).parent().addClass("active");
	});


	/*Modelagem do avatar*/
	$("#bg .ac-box").click(function(){
		$("#bg *").removeClass("ac-selected");
		$(this).addClass("ac-selected");
		$("#avatar-bg").attr("class",$(this).attr("id"));
	});

	$("#face .ac-box").click(function(){
		$("#face *").removeClass("ac-selected");
		$(this).addClass("ac-selected");
		$("#avatar-face").attr("class",$(this).attr("id"));
	});

	$("#olhos .ac-box").click(function(){
		$("#olhos *").removeClass("ac-selected");
		$(this).addClass("ac-selected");
		$("#avatar-olhos").attr("class",$(this).attr("id"));
	});

	$("#nariz .ac-box").click(function(){
		$("#nariz *").removeClass("ac-selected");
		$(this).addClass("ac-selected");
		$("#avatar-nariz").attr("class",$(this).attr("id"));
	});

	$("#boca .ac-box").click(function(){
		$("#boca *").removeClass("ac-selected");
		$(this).addClass("ac-selected");
		$("#avatar-boca").attr("class",$(this).attr("id"));
	});

	$("#cabelo .ac-box").click(function(){
		$("#cabelo *").removeClass("ac-selected");
		$(this).addClass("ac-selected");
		$("#avatar-cabelo").attr("class",$(this).attr("id"));
	});

	$("#roupa .ac-box").click(function(){
		$("#roupa *").removeClass("ac-selected");
		$(this).addClass("ac-selected");
		$("#avatar-roupa").attr("class",$(this).attr("id"));
	});

	$("#acessorio .ac-box").click(function(){
		$("#acessorio *").removeClass("ac-selected");
		$(this).addClass("ac-selected");
		$("#avatar-acessorio").attr("class",$(this).attr("id"));
	});
});